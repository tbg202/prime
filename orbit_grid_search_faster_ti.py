######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

from re import T
from binary_disks_vector import binary_disks_vector
import numpy as np
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from lmfit import Minimizer, Parameters
eachindex = lambda lst: range(len(lst))
from tqdm import tqdm
import os
import matplotlib.cm as cm
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib as mp
from PyAstronomy import pyasl
ks=pyasl.MarkleyKESolver()
from scipy.signal import medfilt
from multiprocessing import Pool

## function to compute expected sep,PA for each time
def companion_position(params,time):
    P = params['P']
    T = params['T']
    e = params['e']
    A = params['A']
    B = params['B']
    F = params['F']
    G = params['G']

    M = 2*np.pi/P*(time-T)
    E = ks.getE(M,e)

    X = np.cos(E)-e
    Y = np.sqrt(1-e**2)*np.sin(E)
    dec = A*X+F*Y
    ra = B*X+G*Y
    return ra,dec

def companion_position_circular(params,time):
    P = params['P']
    T = params['T']
    A = params['A']
    B = params['B']
    F = params['F']
    G = params['G']

    E = 2*np.pi/P*(time-T)

    X = np.cos(E)
    Y = np.sin(E)
    dec = A*X+F*Y
    ra = B*X+G*Y
    return ra,dec

## function which returns complex vis given sep, pa, flux ratio, HA, dec, UD1, UD2, wavelength
def cvis_model(params, u, v, wl, time,circular='n'):
    if circular=='y':
        ra = companion_position_circular(params,time)[0]
        dec = companion_position_circular(params,time)[1]
    else:
        ra = companion_position(params,time)[0]
        dec = companion_position(params,time)[1]
    ratio = params['ratio']
    bw = params['bw']
    ud = params['ud']
    ud2 = params['ud2']
    
    ul=np.array([u/i for i in wl])
    vl=np.array([v/i for i in wl])
    
    vis=binary_disks_vector().binary2(ul,vl,ra,dec,ratio,ud,ud2,bw)
    return vis

## function which returns residual of model and data to be minimized
def cp_minimizer(params,cp,cp_err,u_coords,v_coords,wl,time,circular='n'):
    model=[]
    for item1,item2,item4,item5 in zip(u_coords,v_coords,time,wl):
        complex_vis = cvis_model(params, item1, item2, item5, item4,circular)
        phase = (np.angle(complex_vis[:,0])+np.angle(complex_vis[:,1])+np.angle(complex_vis[:,2]))
        model.append(phase)
    model=np.array(model)

    ## need to do an "angle difference"
    data = cp*np.pi/180
    err = cp_err*np.pi/180
    
    diff = np.arctan2(np.sin(data-model),np.cos(data-model))
    return diff/err

#def planet_grid_compute(a_try,i_try,bigw_try,params,t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular):
def planet_grid_compute(all_params):
    import time
    time.sleep(.1)

    T_try,params,t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular = all_params

    params['T'].value = T_try

    #do fit, minimizer uses LM for least square fitting of model to data
    minner = Minimizer(cp_minimizer, params, fcn_args=(t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular),nan_policy='omit')
    result = minner.minimize()
    #result = minner.leastsq(xtol=1e-5,ftol=1e-5)
    params_list = [result.redchi,result.params['P'].value,result.params['T'].value,
                            result.params['e'].value,result.params['A'].value,
                            result.params['B'].value,result.params['F'].value,
                            result.params['G'].value,result.params['ratio'].value]
    #print(params_list)
    return params_list

#def planet_grid_wrapper(indices):
#    return params_list
#    params_list = planet_grid_compute(*indices)

if __name__ == '__main__':

    ## Ask the user which directory contains all files
    dir=input('Path to oifits directory: ')
    target_id=input('Target ID (e.g. HD_206901): ')
    date = input('Date: ')
    circular = input('Assume circular (y,[n])')

    ## get information from fits file
    t3phi = []
    t3phierr = []
    u_coords = []
    v_coords = []
    eff_wave = []
    time_obs=[]
    #tels = []

    ftype = input('chara/chara_old/vlti? ')

    #new data:
    beam_map = {1:'S1',2:'S2',3:'E1',4:'E2',5:'W1',6:'W2'}
    #older data:
    #beam_map = {0:'S1',1:'S2',2:'E1',3:'E2',4:'W1',5:'W2'}

    #start = 2
    #end = 30
    #start = 1
    #end = 7

    if ftype=='chara':
        for file in sorted(os.listdir(dir)):
            ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
            if file.endswith("_oifits.fits") or file.endswith("_viscal.fits") or file.endswith("_uvfix.fits"):

                filename = os.path.join(dir, file)
                hdu = fits.open(filename)
                oi_target = hdu[0].header['OBJECT']

                ## Check if oifits file is your target of interest
                if oi_target==target_id:
                    #print(filename)
                    oi_mjd = hdu[0].header['MJD-OBS']
                    oi_t3 = hdu['OI_T3'].data
                    oi_vis2 = hdu['OI_VIS2'].data

                    ## t3phi data:
                    for i in eachindex(oi_t3):
                        t3 = oi_t3[i]['T3PHI']
                        t3err = oi_t3[i]['T3PHIERR']
                        t3flag = np.where(oi_t3[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi.append(t3)
                        t3phierr.append(t3err)
                        #tels.append([beam_map[a] for a in oi_t3[i]['STA_INDEX']])
                        u1coord = oi_t3[i]['U1COORD']
                        v1coord = oi_t3[i]['V1COORD']
                        u2coord = oi_t3[i]['U2COORD']
                        v2coord = oi_t3[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords.append([u1coord,u2coord,u3coord])
                        v_coords.append([v1coord,v2coord,v3coord])

                        eff_wave.append(hdu['OI_WAVELENGTH'].data['EFF_WAVE'])
                        time_obs.append(oi_mjd)
                hdu.close()
        t3phi = np.array(t3phi)
        t3phierr = np.array(t3phierr)
        u_coords = np.array(u_coords)
        v_coords = np.array(v_coords)
        eff_wave = np.array(eff_wave)
        time_obs = np.array(time_obs)
    if ftype=='chara_old':
        try:
            hdu = fits.open(dir)
            for table in hdu:

                #if table.name=='OI_WAVELENGTH':
                #    eff_wave.append(table.data['EFF_WAVE'][1:33])

                ## t3phi data:
                wl_i=1
                if table.name=='OI_T3':
                    for i in eachindex(table.data):
                        t3 = table.data[i]['T3PHI']
                        t3err = table.data[i]['T3PHIERR']
                        t3flag = np.where(table.data[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi.append(t3)
                        t3phierr.append(t3err)
                        #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                        u1coord = table.data[i]['U1COORD']
                        v1coord = table.data[i]['V1COORD']
                        u2coord = table.data[i]['U2COORD']
                        v2coord = table.data[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords.append([u1coord,u2coord,u3coord])
                        v_coords.append([v1coord,v2coord,v3coord])
                        time_obs.append(table.data[i]['MJD'])
                        eff_wave.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                    wl_i+=1

            hdu.close()
            t3phi = np.array(t3phi)
            t3phierr = np.array(t3phierr)
            u_coords = np.array(u_coords)
            v_coords = np.array(v_coords)
            eff_wave = np.array(eff_wave)
            time_obs = np.array(time_obs)
        except:
            for file in sorted(os.listdir(dir)):
                #print(file)
                ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
                if file.endswith(".oifits"):

                    filename = os.path.join(dir, file)
                    print(filename)
                    hdu = fits.open(filename)

                    for table in hdu:

                        #if table.name=='OI_WAVELENGTH':
                        #    eff_wave.append(table.data['EFF_WAVE'][1:33])
                        ## t3phi data:
                        wl_i=1
                        if table.name=='OI_T3':
                            for i in eachindex(table.data):
                                t3 = table.data[i]['T3PHI']
                                t3err = table.data[i]['T3PHIERR']
                                t3flag = np.where(table.data[i].field('FLAG')==True)
                                t3[t3flag] = np.nan
                                t3err[t3flag] = np.nan
                                t3phi.append(t3)
                                t3phierr.append(t3err)
                                #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                                u1coord = table.data[i]['U1COORD']
                                v1coord = table.data[i]['V1COORD']
                                u2coord = table.data[i]['U2COORD']
                                v2coord = table.data[i]['V2COORD']
                                u3coord = -u1coord - u2coord
                                v3coord = -v1coord - v2coord
                                u_coords.append([u1coord,u2coord,u3coord])
                                v_coords.append([v1coord,v2coord,v3coord])
                                time_obs.append(table.data[i]['MJD'])
                                eff_wave.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                            wl_i+=1

                    hdu.close()
            t3phi = np.array(t3phi)
            t3phierr = np.array(t3phierr)
            u_coords = np.array(u_coords)
            v_coords = np.array(v_coords)
            eff_wave = np.array(eff_wave)
            time_obs = np.array(time_obs)


    print(t3phi.shape)
    print(u_coords.shape)
    print(eff_wave.shape)
    print(time_obs.shape)

    ### get rid of crazy values
    #idx = np.where(t3phierr>(3*np.nanmean(t3phierr)))
    #t3phi[idx]=np.nan
    #t3_std = np.nanstd(t3phi)
    #print('Standard deviation t3phi = ',t3_std)
    #idx = np.where(abs(t3phi)>(5*t3_std))
    #t3phi[idx]=np.nan

    print(t3phi.shape,t3phierr.shape)
    print(u_coords.shape)
    print(eff_wave.shape)
    print(time_obs.shape)

    correct = input('Correct t3phi? (y/[n]): ')
    if correct == 'y':
        correction_file = input('File with corrected t3phi: ')
        t3phi_corrected = np.load(correction_file)
        print(t3phi.shape)
        print('Using corrected t3phi')
        print(t3phi_corrected.shape)
        t3phi = t3phi_corrected

    #else:
    #    nloop = 0
    #    while nloop<3:
    #        t3phi_filtered = medfilt(t3phi,(1,5))
    #        t3phi_resid = t3phi-t3phi_filtered
    #        std_t3phi = np.nanstd(t3phi_resid)
    #        idx_t3phi = np.where(abs(t3phi_resid)>(5*std_t3phi))
    #        t3phi[idx_t3phi]=np.nan
    #        nloop+=1
    std_t3phi = np.nanstd(t3phi)
    print('Standard deviation t3phi = ',std_t3phi)

    ### average channels
    #avg_channels = input('Average channels together? (y/[n]): ')
    #if avg_channels=='y':
    #    t3phi_filtered = []
    #    t3phierr_filtered = []
    #    eff_wave_filtered = []
    #    for phi,err,wl in zip(t3phi,t3phierr,eff_wave):
    #        t3phi_filtered.append(np.nanmean(phi.reshape(-1,4),axis=1))
    #        #t3phi_filtered.append(np.nanmedian(phi.reshape(-1,4),axis=1))
    #        t3phierr_filtered.append(np.nanmedian(err.reshape(-1,4),axis=1))
    #        #t3phierr_filtered.append(np.nanstd(phi.reshape(-1,4),axis=1))
    #        eff_wave_filtered.append(np.nanmean(wl.reshape(-1,4),axis=1))
    #    t3phi=np.array(t3phi_filtered)
    #    t3phierr=np.array(t3phierr_filtered)
    #    eff_wave=np.array(eff_wave_filtered)

    print('Minimum error = %s'%np.nanmin(t3phierr))
    print('Maximum error = %s'%np.nanmax(t3phierr))
    #rid_error = input("Put a floor on low error bars? (y/[n]): ")

    #if rid_error == 'y':

    #    #error_val = float(input("Minimum error value (deg): "))
    #    #idx = np.where(t3phierr<error_val)
    #    #t3phi[idx]=np.nan

    #    print('Setting a floor on error bars')
    #    #t3phi_10 = np.nanpercentile(t3phierr,0.5)
    #    #t3phi_90 = np.nanpercentile(t3phierr,99.5)

    #    plt.hist(np.ndarray.flatten(t3phierr),bins=100)
    #    plt.show()
    #    error_median = np.nanmedian(t3phierr)
    #    error_floor = error_median/2
    #    print('Error floor = ',error_floor)

    #    idx1 = np.where(t3phierr<error_floor)
    #    #idx2 = np.where(t3phierr>t3phi_90)
    #    t3phierr[idx1]=error_floor
    #    #t3phierr[idx2]=np.nan
    #    #t3phi[idx1]=np.nan
    #    #t3phi[idx2]=np.nan

    #    print('New minimum error = %s'%np.nanmin(t3phierr))
    #    print('New Maximum error = %s'%np.nanmax(t3phierr))

    ###########
    ## Now give orbital elements
    ###########
    #P_guess=float(input('period (days):'))
    #T_guess=float(input('tper (MJD):'))
    #e_guess=float(input('eccentricity:'))
    #omega_guess=float(input('omega (deg):'))
    #ratio_guess = float(input('flux ratio (f1/f2): '))
    #bw_guess = float(input('bw smearing (0.004): '))
    #ud_guess = float(input('UD (mas): '))

    ## Ups And b
    print("Using UPS AND b orbital elements")
    P_guess=4.617111
    T_guess= 50033.55 #59507.86
    e_guess=0.012
    omega_guess=224.11
    a_guess=4.43
    ratio_guess = 3000
    bw_guess = 0.02
    ud_guess = 1.097
    ud2_guess = 0.1
    ## semi-major should be 4.4mas
    ## inclination 24deg (Piskorz et al, 2017)

    ### Tau Boo b
    #print("Using TAU BOO b orbital elements")
    #P_guess=3.3124568
    #T_guess= 56400.44 #59673.15
    #e_guess=0.011
    #omega_guess=113.4
    #ratio_guess = 2500
    #bw_guess = 0.0
    #ud_guess = 0.814
    #ud2_guess = 0.1
    ## semi-major should be 3.1mas
    ## inclination 43.5deg (Pelletier et al, 2021)

    ## 51 Peg b
    #print("Using 51 Peg b orbital elements")
    #P_guess=4.231
    #T_guess= 56020.756
    #e_guess=0.0
    #omega_guess=0.0
    #ratio_guess = 3000
    #bw_guess = 0.02
    #ud_guess = 0.4
    #ud2_guess = 0.1
    ## semi-major should be 3.5mas
    ## inclination 80? (Martins et al, 2015)

    ###########
    ## Now compute chi-sq
    ###########
    T_start = T_guess - P_guess/2
    T_end = T_guess + P_guess/2
    T_grid = np.linspace(T_start,T_end,25)

    #create a set of Parameters, choose starting value and range for search
    params = Parameters()

    params.add('P', value=P_guess, vary=False)
    params.add('T', value=1, vary=False)
    if circular=='y':
        params.add('e', value=0.0, vary=False)
    else:
        params.add('e', value=e_guess, vary=False)

    params.add('A', value=0)#min=0)
    params.add('B', value=0)#min=0)
    params.add('F', value=0)#min=0, max=360)
    params.add('G', value=0)#min=0, max=360)

    params.add('bw', value=0, vary=False)
    params.add('ud', value=ud_guess, vary=False)
    params.add('ud2', value=ud2_guess, vary=False)
    params.add('ratio', value=ratio_guess, min=1.0)

    #
    #for bigw_idx,bigw_try in enumerate(tqdm(bigw_grid)):
    #    for i_idx,i_try in enumerate(i_grid):
    #        for a_idx,a_try in enumerate(a_grid):
    #            
    #            result = planet_grid_compute(params,bigw_try,i_try,a_try,t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular)
    #
    #            chi_sq[a_idx,i_idx,bigw_idx] = result.redchi
    #            bigw_results[a_idx,i_idx,bigw_idx] = result.params['bigw'].value
    #            a_results[a_idx,i_idx,bigw_idx] = result.params['a'].value
    #            i_results[a_idx,i_idx,bigw_idx] = result.params['inc'].value
    #            ratio_results[a_idx,i_idx,bigw_idx] = result.params['ratio'].value
    #
    #            P_results[a_idx,i_idx,bigw_idx] = result.params['P'].value
    #            T_results[a_idx,i_idx,bigw_idx] = result.params['T'].value
    #            e_results[a_idx,i_idx,bigw_idx] = result.params['e'].value
    #            w_results[a_idx,i_idx,bigw_idx] = result.params['omega'].value
    #
    chi_sq = np.zeros(shape=(len(T_grid)))
    P_results = np.zeros(shape=(len(T_grid)))
    T_results = np.zeros(shape=(len(T_grid)))
    e_results = np.zeros(shape=(len(T_grid)))
    A_results = np.zeros(shape=(len(T_grid)))
    B_results = np.zeros(shape=(len(T_grid)))
    F_results = np.zeros(shape=(len(T_grid)))
    G_results = np.zeros(shape=(len(T_grid)))
    ratio_results = np.zeros(shape=(len(T_grid)))

    results = pool.map(planet_grid_compute, [(T_try,i_try,bigw_try,params,t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular) for bigw_try in bigw_grid])
    pool = Pool()
    for T_idx,T_try in enumerate(tqdm(T_grid)):
        results = pool.map(planet_grid_compute, [T_try,params,t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,circular])
        results = np.array(results)
        
        chi_sq[T_idx] = results[0]
        P_results[T_idx] = results[1]
        T_results[T_idx] = results[2]
        e_results[T_idx] = results[3]
        A_results[T_idx] = results[4]
        B_results[T_idx] = results[5]
        F_results[T_idx] = results[6]
        G_results[T_idx] = results[7]
        ratio_results[T_idx] = results[8]

    # write results
    np.save('./orbitsearch_results/P_results_%s_%s.npy'%(target_id,date), P_results)
    np.save('./orbitsearch_results/T_results_%s_%s.npy'%(target_id,date), T_results)
    np.save('./orbitsearch_results/e_results_%s_%s.npy'%(target_id,date), e_results)
    np.save('./orbitsearch_results/A_results_%s_%s.npy'%(target_id,date), A_results)
    np.save('./orbitsearch_results/B_results_%s_%s.npy'%(target_id,date), B_results)
    np.save('./orbitsearch_results/F_results_%s_%s.npy'%(target_id,date), F_results)
    np.save('./orbitsearch_results/G_results_%s_%s.npy'%(target_id,date), G_results)
    np.save('./orbitsearch_results/ratio_results_%s_%s.npy'%(target_id,date), ratio_results)
    np.save('./orbitsearch_results/chi_sq_%s_%s.npy'%(target_id,date), chi_sq)


    ## plot results
    with PdfPages("./orbitsearch_results/%s_%s_orbitsearch.pdf"%(target_id,date)) as pdf:
        ## first page - chisq grid of a and i
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.plot(T_results, 1/chi_sq, '.')
        plt.xlabel('T (MJD)')
        plt.ylabel('1/chi2')
        #plt.title('Best Fit - %s'%best_fit)
        #plt.axis('equal')
        pdf.savefig()
        plt.close()

        ### second page - chisq grid of bigw and i
        #fig = plt.figure()
        #ax = fig.add_subplot(111)
        #plt.scatter(bigw_results[T_idx,:,:], i_results[T_idx,:,:], c=1/chi_sq[T_idx,:,:], cmap=cm.inferno, s=150)
        #plt.colorbar()
        #ax.set_xlim(np.nanmin(bigw_results[T_idx,:,:]),np.nanmax(bigw_results[T_idx,:,:]))
        #ax.set_ylim(np.nanmin(i_results[T_idx,:,:]),np.nanmax(i_results[T_idx,:,:]))
        #plt.xlabel('bigw (deg)')
        #plt.ylabel('i (deg)')
        ##plt.title('Best Fit - %s'%best_fit)
        ##plt.axis('equal')
        #pdf.savefig()
        #plt.close()
