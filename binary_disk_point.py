## For a binary star with UD for one and point for other
##
## Inputs: u,v,separation,position angle,flux ratio,UD
## NOTE: u,v should be in units of wavelength (i.e. u/lambda)
## NOTE: companion is unresolved here
##
## Outputs: complex visibility (amp and phase)

import numpy as np
import itertools
from scipy import special
from itertools import combinations

class binary_disk_point:

    def mas2rad(self,mas):
        rad=mas/1000.
        rad=rad/3600.
        rad=rad*np.pi/180.
        return(rad)

    def binary(self,u,v,sep,pa,ratio,ud,bw):
        delta_dec=self.mas2rad(sep*np.sin((pa+90)*np.pi/180))
        delta_ra=-self.mas2rad(sep*np.cos((pa+90)*np.pi/180))
        
        secondary_flux = 1/(1+ratio)
        primary_flux = 1-secondary_flux
        
        diameter = self.mas2rad(ud)
        x = np.pi*diameter*np.sqrt(u**2+v**2)
    
        ## bessel 1st order
        f = 2*special.jn(1,x)/x

        ## bandwidth smearing
        vis_bw = np.sinc(bw*(u*delta_ra+v*delta_dec))
                        
        #complex_vis=(f+ratio*c)/(1+ratio)
        c = np.exp(-2*np.pi*1j*(u*delta_ra+v*delta_dec))
        complex_vis = primary_flux*f + vis_bw*secondary_flux*c
                         
        return(complex_vis)

    def binary2(self,u,v,ra,dec,ratio,ud,bw):
        delta_dec=self.mas2rad(dec)
        delta_ra=self.mas2rad(ra)
        
        secondary_flux = 1/(1+ratio)
        primary_flux = 1-secondary_flux
        
        diameter = self.mas2rad(ud)
        x = np.pi*diameter*np.sqrt(u**2+v**2)
    
        ## bessel 1st order
        f = 2*special.jn(1,x)/x

        ## bandwidth smearing
        vis_bw = np.sinc(bw*(u*delta_ra+v*delta_dec))
                        
        #complex_vis=(f+ratio*c)/(1+ratio)
        c = np.exp(-2*np.pi*1j*(u*delta_ra+v*delta_dec))
        complex_vis = primary_flux*f + vis_bw*secondary_flux*c
                         
        return(complex_vis)
