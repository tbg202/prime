######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.backends.backend_pdf import PdfPages

#a_file = np.load(input('semi file: '))
i_file = np.load(input('inc file: '))
bigw_file = np.load(input('bigw file: '))
chi_file = np.load(input('chi2 file: '))

target_id = input('Target: ')
date = input('Note: ')

chi2_single = float(input('Reduced chi2 of single: '))
dof = float(input('DOF: '))

#print(a_file.shape)
print(i_file.shape)
print(bigw_file.shape)
print(chi_file.shape)

q = scipy.stats.chi2.cdf(dof*chi2_single/chi_file, dof)
p = 1.0-q
nsigma = np.sqrt(scipy.stats.chi2.ppf(1-p, 1))
if isinstance(nsigma, np.ndarray):
    nsigma[p<1e-15] = np.sqrt(scipy.stats.chi2.ppf(1-1e-15, 1))
elif p<1e-15:
    nsigma = np.sqrt(scipy.stats.chi2.ppf(1-1e-15, 1))

x=np.unique(bigw_file)
y=np.unique(i_file)
X,Y = np.meshgrid(x,y)

## plot results
with PdfPages("./orbitsearch_results/%s_%s_bigw_grid.pdf"%(target_id,date)) as pdf:
    ## first page - chisq grid
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #plt.scatter(bigw_file, i_file, c=nsigma, cmap=cm.inferno, s = 150)
    plt.pcolormesh(X,Y,nsigma,edgecolors='face')
    plt.colorbar()
    ax.set_xlim(np.min(bigw_file),np.max(bigw_file))
    #ax.set_xlim(40,60)
    ax.set_ylim(np.min(i_file),np.max(i_file))
    plt.xlabel('$\Omega$ (deg)')
    plt.ylabel('i (deg)')
    #plt.title('Best Fit - %s'%best_fit)
    #plt.axis('equal')
    pdf.savefig()
    plt.close()
