######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

from chara_uvcalc import uv_calc
from binary_disk_point import binary_disk_point
from binary_disks_vector import binary_disks_vector
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
import astropy.io.fits as fits
from lmfit import minimize, Minimizer, Parameters, Parameter, report_fit
eachindex = lambda lst: range(len(lst))
from tqdm import tqdm
import os
import matplotlib.cm as cm
import time
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib as mp
from PyAstronomy import pyasl
ks=pyasl.MarkleyKESolver()
from scipy.signal import medfilt
from scipy import stats

## function which returns complex vis given sep, pa, flux ratio, HA, dec, UD1, UD2, wavelength
def cvis_model(params, u, v, wl, time,circular='n'):
    
    ratio = params['ratio']
    bw = params['bw']
    ud = params['ud']
    ud2 = params['ud2']
    
    ra = 0
    dec = 0
    
    ul=np.array([u/i for i in wl])
    vl=np.array([v/i for i in wl])

    vis=binary_disks_vector().binary2(ul,vl,ra,dec,ratio,ud,ud2,bw)
    return vis

## function which returns residual of model and data to be minimized
def cp_minimizer(params,cp,cp_err,u_coords,v_coords,wl,time,nruns,circular='n'):
    model = np.empty(nruns,dtype=object)
    for obs1,obs2,obs4,obs5,run in zip(u_coords,v_coords,time,wl,np.arange(nruns)):
        model_obs = []
        for item1,item2,item4,item5 in zip(obs1,obs2,obs4,obs5):
            complex_vis = cvis_model(params, item1, item2, item5, item4,circular)
            phase = (np.angle(complex_vis[:,0])+np.angle(complex_vis[:,1])+np.angle(complex_vis[:,2]))
            model_obs.append(phase)
        model[run] = np.array(model_obs)

    ## need to do an "angle difference"
    data = cp*np.pi/180
    err = cp_err*np.pi/180

    diff = np.empty(shape=0)
    for m,d,e in zip(model,data,err):
        resid = np.arctan2(np.sin(d-m),np.cos(d-m))/e
        resid = np.ndarray.flatten(resid)
        diff = np.append(diff,resid,axis=0)
    return np.array(diff)

## get information from fits file
number_runs = int(input('Number of runs to load = '))
t3phi = np.empty(number_runs,dtype=object)
t3phierr = np.empty(number_runs,dtype=object)
u_coords = np.empty(number_runs,dtype=object)
v_coords = np.empty(number_runs,dtype=object)
eff_wave = np.empty(number_runs,dtype=object)
time_obs = np.empty(number_runs,dtype=object)
#tels = []

#new data:
beam_map = {1:'S1',2:'S2',3:'E1',4:'E2',5:'W1',6:'W2'}
#older data:
#beam_map = {0:'S1',1:'S2',2:'E1',3:'E2',4:'W1',5:'W2'}

#start = 2
#end = 30
#start = 1
#end = 7

for run in np.arange(number_runs):
    print('--'*10)
    print('--'*10)
    print("Load Run %s"%run)
    ## Ask the user which directory contains all files
    dir=input('Path to oifits directory: ')
    ftype = input('chara/chara_old? ')
    t3phi_run = []
    t3phierr_run = []
    u_coords_run = []
    v_coords_run = []
    eff_wave_run = []
    time_obs_run = []
    if ftype=='chara':
        for file in sorted(os.listdir(dir)):
            ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
            if file.endswith("_oifits.fits") or file.endswith("_viscal.fits") or file.endswith("_uvfix.fits"):
                
                filename = os.path.join(dir, file)
                hdu = fits.open(filename)
                oi_target = hdu[0].header['OBJECT']
                
                ## Check if oifits file is your target of interest
                if oi_target==target_id:
                    #print(filename)
                    oi_mjd = hdu[0].header['MJD-OBS']
                    oi_t3 = hdu['OI_T3'].data
                    oi_vis2 = hdu['OI_VIS2'].data
                    
                    ## t3phi data:
                    for i in eachindex(oi_t3):
                        t3 = oi_t3[i]['T3PHI']
                        t3err = oi_t3[i]['T3PHIERR']
                        t3flag = np.where(oi_t3[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi_run.append(t3)
                        t3phierr_run.append(t3err)
                        #tels.append([beam_map[a] for a in oi_t3[i]['STA_INDEX']])
                        u1coord = oi_t3[i]['U1COORD']
                        v1coord = oi_t3[i]['V1COORD']
                        u2coord = oi_t3[i]['U2COORD']
                        v2coord = oi_t3[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords_run.append([u1coord,u2coord,u3coord])
                        v_coords_run.append([v1coord,v2coord,v3coord])
                        
                        eff_wave_run.append(hdu['OI_WAVELENGTH'].data['EFF_WAVE'])
                        time_obs_run.append(oi_mjd)
                hdu.close()
    else:
        try:
            hdu = fits.open(dir)
            for table in hdu:
                
                #if table.name=='OI_WAVELENGTH':
                #    eff_wave.append(table.data['EFF_WAVE'][1:33])
                
                ## t3phi data:
                wl_i=1
                if table.name=='OI_T3':
                    for i in eachindex(table.data):
                        t3 = table.data[i]['T3PHI']
                        t3err = table.data[i]['T3PHIERR']
                        t3flag = np.where(table.data[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi_run.append(t3)
                        t3phierr_run.append(t3err)
                        #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                        u1coord = table.data[i]['U1COORD']
                        v1coord = table.data[i]['V1COORD']
                        u2coord = table.data[i]['U2COORD']
                        v2coord = table.data[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords_run.append([u1coord,u2coord,u3coord])
                        v_coords_run.append([v1coord,v2coord,v3coord])
                        time_obs_run.append(table.data[i]['MJD'])
                        eff_wave_run.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                    wl_i+=1
        
            hdu.close()
        except:
            for file in sorted(os.listdir(dir)):
                #print(file)
                ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
                if file.endswith(".oifits"):
                    
                    filename = os.path.join(dir, file)
                    print(filename)
                    hdu = fits.open(filename)
                    
                    for table in hdu:
                        
                        #if table.name=='OI_WAVELENGTH':
                        #    eff_wave.append(table.data['EFF_WAVE'][1:33])
                        ## t3phi data:
                        wl_i=1
                        if table.name=='OI_T3':
                            for i in eachindex(table.data):
                                t3 = table.data[i]['T3PHI']
                                t3err = table.data[i]['T3PHIERR']
                                t3flag = np.where(table.data[i].field('FLAG')==True)
                                t3[t3flag] = np.nan
                                t3err[t3flag] = np.nan
                                t3phi_run.append(t3)
                                t3phierr_run.append(t3err)
                                #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                                u1coord = table.data[i]['U1COORD']
                                v1coord = table.data[i]['V1COORD']
                                u2coord = table.data[i]['U2COORD']
                                v2coord = table.data[i]['V2COORD']
                                u3coord = -u1coord - u2coord
                                v3coord = -v1coord - v2coord
                                u_coords_run.append([u1coord,u2coord,u3coord])
                                v_coords_run.append([v1coord,v2coord,v3coord])
                                time_obs_run.append(table.data[i]['MJD'])
                                eff_wave_run.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                            wl_i+=1
                
                    hdu.close()
    
    t3phi_run = np.array(t3phi_run)
    t3phierr_run = np.array(t3phierr_run)
    u_coords_run = np.array(u_coords_run)
    v_coords_run = np.array(v_coords_run)
    eff_wave_run = np.array(eff_wave_run)
    time_obs_run = np.array(time_obs_run)

    print(t3phi_run.shape)
    print(u_coords_run.shape)
    print(eff_wave_run.shape)
    print(time_obs_run.shape)
    
    ### get rid of crazy values
    #idx = np.where(t3phierr>(3*np.nanmean(t3phierr)))
    #t3phi[idx]=np.nan
    #t3_std = np.nanstd(t3phi)
    #print('Standard deviation t3phi = ',t3_std)
    #idx = np.where(abs(t3phi)>(5*t3_std))
    #t3phi[idx]=np.nan
    
    ## check for t3phi corrections based on cals (another script)
    #if ftype=='chara' or ftype=='chara_old':
    #    t3phi_corrected=np.empty((0,t3phi.shape[-1]))
    #    t3phierr_corrected=np.empty((0,t3phierr.shape[-1]))
    #    eff_wave_corrected=np.empty((0,eff_wave.shape[-1]))
    #    u_coords_corrected=np.empty((0,u_coords.shape[-1]))
    #    v_coords_corrected=np.empty((0,v_coords.shape[-1]))
    #    time_obs_corrected=np.empty((0))
    #    for file in os.listdir(dir):
    #        if file.endswith("npy"):
    #            filename = os.path.join(dir, file)
    #            t3 = np.load(filename,allow_pickle=True)
    #            print(t3.shape)
    #            t3phi_corrected = np.append(t3phi_corrected,t3,axis=0)
    #            t3phierr_corrected = np.append(t3phierr_corrected,t3[1],axis=0)
    #            eff_wave_corrected = np.append(eff_wave_corrected,t3[2],axis=0)
    #            u_coords_corrected = np.append(u_coords_corrected,t3[3],axis=0)
    #            v_coords_corrected = np.append(v_coords_corrected,t3[4],axis=0)
    #            time_obs_corrected = np.append(time_obs_corrected,t3[5],axis=0)
    #    if t3phi_corrected.shape[0]>0:
    #        t3phi = t3phi_corrected
    #        t3phierr = t3phierr_corrected
    #        eff_wave = eff_wave_corrected
    #        u_coords = u_coords_corrected
    #        v_coords = v_coords_corrected
    #        time_obs = time_obs_corrected
    #        t3phi = np.concatenate([t3phi,t3phi_corrected])
    #        t3phierr = np.concatenate([t3phierr,t3phierr_corrected])
    #        eff_wave = np.concatenate([eff_wave,eff_wave_corrected])
    #        u_coords = np.concatenate([u_coords,u_coords_corrected])
    #        v_coords = np.concatenate([v_coords,v_coords_corrected])
    #        time_obs = np.concatenate([time_obs,time_obs_corrected])
    #        print('Using CORRECTED t3phi')
    #
    #    print(t3phi.shape)
    #    print(u_coords.shape)
    #    print(eff_wave.shape)
    #    print(time_obs.shape)
    correct = input('Correct t3phi? (y/[n]): ')
    if correct == 'y':
        correction_file = input('File with corrected t3phi: ')
        t3phi_corrected = np.load(correction_file)
        print(t3phi_run.shape)
        print(t3phi_corrected.shape)
        print('Using corrected t3phi')
        t3phi_run = t3phi_corrected
    std_t3phi = np.nanstd(t3phi_run)
    print('Standard deviation t3phi = ',std_t3phi)
    
    ## average channels
    avg_channels = input('Average channels together? (y/n): ')
    if avg_channels=='y':
        t3phi_filtered = []
        t3phierr_filtered = []
        eff_wave_filtered = []
        for phi,err,wl in zip(t3phi_run,t3phierr_run,eff_wave_run):
            t3phi_filtered.append(np.nanmean(phi.reshape(-1,4),axis=1))
            #t3phi_filtered.append(np.nanmedian(phi.reshape(-1,4),axis=1))
            t3phierr_filtered.append(np.nanmedian(err.reshape(-1,4),axis=1))
            #t3phierr_filtered.append(np.nanstd(phi.reshape(-1,4),axis=1))
            eff_wave_filtered.append(np.nanmean(wl.reshape(-1,4),axis=1))
        t3phi_run=np.array(t3phi_filtered)
        t3phierr_run=np.array(t3phierr_filtered)
        eff_wave_run=np.array(eff_wave_filtered)
    
    print('Minimum error = %s'%np.nanmin(t3phierr_run))
    print('Maximum error = %s'%np.nanmax(t3phierr_run))
    rid_error = input("Put a floor on low error bars? (y/[n]): ")
    
    if rid_error == 'y':
        
        #error_val = float(input("Minimum error value (deg): "))
        #idx = np.where(t3phierr<error_val)
        #t3phi[idx]=np.nan
        
        print('Setting a floor on error bars')
        #t3phi_10 = np.nanpercentile(t3phierr,0.5)
        #t3phi_90 = np.nanpercentile(t3phierr,99.5)
        
        plt.hist(np.ndarray.flatten(t3phierr_run),bins=100)
        plt.show()
        error_median = np.nanmedian(t3phierr_run)
        error_floor = error_median/2
        print('Error floor = ',error_floor)
        
        idx1 = np.where(t3phierr_run<error_floor)
        #idx2 = np.where(t3phierr>t3phi_90)
        t3phierr_run[idx1]=error_floor
        #t3phierr[idx2]=np.nan
        #t3phi[idx1]=np.nan
        #t3phi[idx2]=np.nan
        
        print('New minimum error = %s'%np.nanmin(t3phierr_run))
        print('New Maximum error = %s'%np.nanmax(t3phierr_run))

    t3phi_run = np.array(t3phi_run)
    t3phierr_run = np.array(t3phierr_run)
    u_coords_run = np.array(u_coords_run)
    v_coords_run = np.array(v_coords_run)
    eff_wave_run = np.array(eff_wave_run)
    time_obs_run = np.array(time_obs_run)

    #### plot t3phi data - 20 closing triangles for 6 telescopes
    #for t,terr in zip(t3phi,t3phierr):
    #    x=eff_wave[0]*1e6
    #    plt.errorbar(x,t,yerr=terr,fmt='.-')
    #
    #    plt.title('%s Closure Phase'%target_id)
    #    plt.show()
    #    plt.close('all')

    t3phi[run] = t3phi_run
    t3phierr[run] = t3phierr_run
    u_coords[run] = u_coords_run
    v_coords[run] = v_coords_run
    eff_wave[run] = eff_wave_run
    time_obs[run] = time_obs_run

#t3phi = np.array(t3phi)
#t3phierr = np.array(t3phierr)
#u_coords = np.array(u_coords)
#v_coords = np.array(v_coords)
#eff_wave = np.array(eff_wave)
#time_obs = np.array(time_obs)

#t3phi = np.concatenate(t3phi,axis=0)
#t3phierr = np.concatenate(t3phierr,axis=0)
#u_coords = np.concatenate(u_coords,axis=0)
#v_coords = np.concatenate(v_coords,axis=0)
#eff_wave = np.concatenate(eff_wave,axis=0)
#time_obs = np.concatenate(time_obs,axis=0)

## split spectrum
#t3phi = t3phi[:,3:]
#t3phierr = t3phierr[:,3:]
#eff_wave = eff_wave[:,3:]

print('--'*10)
print('--'*10)
print("Total DATA")
print(t3phi.shape)
print(u_coords.shape)
print(eff_wave.shape)
print(time_obs.shape)
print('--'*10)
print('--'*10)

###########
## Now give orbital elements
###########
#P_guess=float(input('period (days):'))
#T_guess=float(input('tper (MJD):'))
#e_guess=float(input('eccentricity:'))
#omega_guess=float(input('omega (deg):'))
#ratio_guess = float(input('flux ratio (f1/f2): '))
#bw_guess = float(input('bw smearing (0.004): '))
#ud_guess = float(input('UD (mas): '))

## Ups And b
#print("Using UPS AND b orbital elements")
#ud_guess = 1.097
#ud2_guess = 0.1

## Tau Boo b
print("Using TAU BOO b orbital elements")
ud_guess = 0.85
ud2_guess = 0.1

## 51 Peg b
#print("Using 51 Peg b orbital elements")
#P_guess=4.230785
#T_guess= 50001.01
#e_guess=0.013
#omega_guess=58+180
#ratio_guess = 3000
#bw_guess = 0.02
#ud_guess = 0.677
#ud2_guess = 0.1
## semi-major should be 3.41mas
## inclination 80? (Martins et al, 2015)

#P_guess=10.21296
#T_guess=52997.6813
#e_guess=0.00198
#omega_guess=102.427
#ratio_guess = 4.6
#bw_guess = 0.005
#ud_guess = 1.05
#ud2_guess = 0.6

###########
## Now compute chi-sq
###########

# test
#nbigw_guess = 1
#bigomega_try = [225]

#create a set of Parameters, choose starting value and range for search
params = Parameters()
params.add('bw', value=0, vary=False)
params.add('ud', value=ud_guess, vary=False)
params.add('ud2', value=ud2_guess, vary=False)
params.add('ratio', value=1e10, vary=False)

minner = Minimizer(cp_minimizer, params, fcn_args=(t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,number_runs),nan_policy='omit')
#result = minner.minimize()
result = minner.leastsq(xtol=1e-5,ftol=1e-5)
#result = minner.minimize(method='dual_annealing')
report_fit(result)
