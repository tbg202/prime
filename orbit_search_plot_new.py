######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

import numpy as np
import scipy
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.backends.backend_pdf import PdfPages

#a_results = np.load(input('semi results: '))
i_results = np.load(input('inc file: '))
bigw_results = np.load(input('bigw file: '))
chi_results = np.load(input('chi2 file: '))
bic_results = np.load(input('bic file: '))
T_results = np.load(input('T file: '))

target_id = input('Target: ')
date = input('Note: ')

index = np.where(chi_results==np.nanmin(chi_results))
T_idx = index[0]
i_idx = index[1]
bigw_idx = index[2]

#### Computing NSIGMA of detection
chi2_single = float(input('Reduced chi2 of single: '))
dof = float(input('DOF: '))
q = scipy.stats.chi2.cdf(dof*chi2_single/chi_results, dof)
p = 1.0-q
nsigma = np.sqrt(scipy.stats.chi2.ppf(1-p, 1))
if isinstance(nsigma, np.ndarray):
    nsigma[p<1e-15] = np.sqrt(scipy.stats.chi2.ppf(1-1e-15, 1))
elif p<1e-15:
    nsigma = np.sqrt(scipy.stats.chi2.ppf(1-1e-15, 1))

## Computing delta_bic of detection
#bic_single = float(input('BIC of single: ' ))
#delta_bic = bic_single - bic_results

x1=np.unique(T_results)
y1=np.unique(i_results)
X1,Y1 = np.meshgrid(x1,y1)

x2=np.unique(bigw_results)
y2=np.unique(i_results)
X2,Y2 = np.meshgrid(x2,y2)

#X1 = X1[:22,:]
#Y1 = Y1[:22,:]
#nsigma = nsigma[:,:22,:]
#X2 = X2[:22,:]
#Y2 = Y2[:22,:]

## plot results
with PdfPages("./orbitsearch_results/%s_%s_orbitsearch.pdf"%(target_id,date)) as pdf:
    ## first page - chisq grid of T and i
    fig = plt.figure()
    ax = fig.add_subplot(111)

    plt.pcolormesh(X1,Y1,nsigma[:,:,bigw_idx[0]].T, edgecolors='face')
    plt.colorbar(label='N Sigma')
    #plt.pcolormesh(X1,Y1,delta_bic[:,:,bigw_idx[0]].T, edgecolors='face')
    #plt.colorbar(label='$\Delta$ BIC')

    ax.set_xlim(np.nanmin(T_results[:,:,bigw_idx[0]]),np.nanmax(T_results[:,:,bigw_idx[0]]))
    ax.set_ylim(np.nanmin(i_results[:,:,bigw_idx[0]]),np.nanmax(i_results[:,:,bigw_idx[0]]))
    
    plt.xlabel('T (MJD)')
    plt.ylabel('inclination (deg)')
    #plt.title('Best Fit - %s'%best_fit)
    #plt.axis('equal')
    pdf.savefig()
    plt.close()
        
    ## second page - chisq grid of bigw and i
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    plt.pcolormesh(X2,Y2,nsigma[T_idx[0],:,:], edgecolors='face')
    plt.colorbar(label='N Sigma')
    #plt.pcolormesh(X2,Y2,delta_bic[T_idx[0],:,:], edgecolors='face')
    #plt.colorbar(label='$\Delta$ BIC')
    
    ax.set_xlim(np.nanmin(bigw_results[T_idx[0],:,:]),np.nanmax(bigw_results[T_idx[0],:,:]))
    ax.set_ylim(np.nanmin(i_results[T_idx[0],:,:]),np.nanmax(i_results[T_idx[0],:,:]))
    plt.xlabel('$\Omega$ (deg)')
    plt.ylabel('inclination (deg)')
    #plt.title('Best Fit - %s'%best_fit)
    #plt.axis('equal')
    pdf.savefig()
    plt.close()
