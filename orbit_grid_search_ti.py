######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

from chara_uvcalc import uv_calc
from binary_disk_point import binary_disk_point
from binary_disks_vector import binary_disks_vector
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
import astropy.io.fits as fits
from lmfit import minimize, Minimizer, Parameters, Parameter, report_fit
eachindex = lambda lst: range(len(lst))
from tqdm import tqdm
import os
import matplotlib.cm as cm
import time
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib as mp
from PyAstronomy import pyasl
ks=pyasl.MarkleyKESolver()
from scipy.signal import medfilt
from scipy import stats

## function to compute expected sep,PA for each time
def companion_position(params,time):
    P = params['P']
    T = params['T']
    e = params['e']
    A = params['A']
    B = params['B']
    F = params['F']
    G = params['G']

    M = 2*np.pi/P*(time-T)
    E = ks.getE(M,e)

    X = np.cos(E)-e
    Y = np.sqrt(1-e**2)*np.sin(E)
    dec = A*X+F*Y
    ra = B*X+G*Y
    return ra,dec

def companion_position_circular(params,time):
    P = params['P']
    T = params['T']
    A = params['A']
    B = params['B']
    F = params['F']
    G = params['G']

    E = 2*np.pi/P*(time-T)

    X = np.cos(E)
    Y = np.sin(E)
    dec = A*X+F*Y
    ra = B*X+G*Y
    return ra,dec

## function which returns complex vis given sep, pa, flux ratio, HA, dec, UD1, UD2, wavelength
def cvis_model(params, u, v, wl, time,circular='n'):
    if circular=='y':
        ra = companion_position_circular(params,time)[0]
        dec = companion_position_circular(params,time)[1]
    else:
        ra = companion_position(params,time)[0]
        dec = companion_position(params,time)[1]
    ratio = params['ratio']
    bw = params['bw']
    ud = params['ud']
    ud2 = params['ud2']
    
    ul=np.array([u/i for i in wl])
    vl=np.array([v/i for i in wl])
    
    vis=binary_disks_vector().binary2(ul,vl,ra,dec,ratio,ud,ud2,bw)
    return vis

## function which returns residual of model and data to be minimized
def cp_minimizer(params,cp,cp_err,u_coords,v_coords,wl,time,nruns,circular='n'):
    model = np.empty(nruns,dtype=object)
    for obs1,obs2,obs4,obs5,run in zip(u_coords,v_coords,time,wl,np.arange(nruns)):
        model_obs = []
        for item1,item2,item4,item5 in zip(obs1,obs2,obs4,obs5):
            complex_vis = cvis_model(params, item1, item2, item5, item4,circular)
            phase = (np.angle(complex_vis[:,0])+np.angle(complex_vis[:,1])+np.angle(complex_vis[:,2]))
            model_obs.append(phase)
        model[run] = np.array(model_obs)

    ## need to do an "angle difference"
    data = cp*np.pi/180
    err = cp_err*np.pi/180

    diff = np.empty(shape=0)
    for m,d,e in zip(model,data,err):
        resid = np.arctan2(np.sin(d-m),np.cos(d-m))/e
        resid = np.ndarray.flatten(resid)
        diff = np.append(diff,resid,axis=0)
    return np.array(diff)

## Info for save files
target_id=input('Target ID (e.g. HD_206901): ')
date = input('Date: ')
circular = input('Assume circular (y,[n])')

## get information from fits file
number_runs = int(input('Number of runs to load = '))
t3phi = np.empty(number_runs,dtype=object)
t3phierr = np.empty(number_runs,dtype=object)
u_coords = np.empty(number_runs,dtype=object)
v_coords = np.empty(number_runs,dtype=object)
eff_wave = np.empty(number_runs,dtype=object)
time_obs = np.empty(number_runs,dtype=object)
#tels = []

#new data:
beam_map = {1:'S1',2:'S2',3:'E1',4:'E2',5:'W1',6:'W2'}
#older data:
#beam_map = {0:'S1',1:'S2',2:'E1',3:'E2',4:'W1',5:'W2'}

#start = 2
#end = 30
#start = 1
#end = 7

for run in np.arange(number_runs):
    print('--'*10)
    print('--'*10)
    print("Load Run %s"%run)
    ## Ask the user which directory contains all files
    dir=input('Path to oifits directory: ')
    ftype = input('chara/chara_old? ')
    t3phi_run = []
    t3phierr_run = []
    u_coords_run = []
    v_coords_run = []
    eff_wave_run = []
    time_obs_run = []
    if ftype=='chara':
        for file in sorted(os.listdir(dir)):
            ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
            if file.endswith("_oifits.fits") or file.endswith("_viscal.fits") or file.endswith("_uvfix.fits"):
                
                filename = os.path.join(dir, file)
                hdu = fits.open(filename)
                oi_target = hdu[0].header['OBJECT']
                
                ## Check if oifits file is your target of interest
                if oi_target==target_id:
                    #print(filename)
                    oi_mjd = hdu[0].header['MJD-OBS']
                    oi_t3 = hdu['OI_T3'].data
                    oi_vis2 = hdu['OI_VIS2'].data
                    
                    ## t3phi data:
                    for i in eachindex(oi_t3):
                        t3 = oi_t3[i]['T3PHI']
                        t3err = oi_t3[i]['T3PHIERR']
                        t3flag = np.where(oi_t3[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi_run.append(t3)
                        t3phierr_run.append(t3err)
                        #tels.append([beam_map[a] for a in oi_t3[i]['STA_INDEX']])
                        u1coord = oi_t3[i]['U1COORD']
                        v1coord = oi_t3[i]['V1COORD']
                        u2coord = oi_t3[i]['U2COORD']
                        v2coord = oi_t3[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords_run.append([u1coord,u2coord,u3coord])
                        v_coords_run.append([v1coord,v2coord,v3coord])
                        
                        eff_wave_run.append(hdu['OI_WAVELENGTH'].data['EFF_WAVE'])
                        time_obs_run.append(oi_mjd)
                hdu.close()
    else:
        try:
            hdu = fits.open(dir)
            for table in hdu:
                
                #if table.name=='OI_WAVELENGTH':
                #    eff_wave.append(table.data['EFF_WAVE'][1:33])
                
                ## t3phi data:
                wl_i=1
                if table.name=='OI_T3':
                    for i in eachindex(table.data):
                        t3 = table.data[i]['T3PHI']
                        t3err = table.data[i]['T3PHIERR']
                        t3flag = np.where(table.data[i].field('FLAG')==True)
                        t3[t3flag] = np.nan
                        t3err[t3flag] = np.nan
                        t3phi_run.append(t3)
                        t3phierr_run.append(t3err)
                        #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                        u1coord = table.data[i]['U1COORD']
                        v1coord = table.data[i]['V1COORD']
                        u2coord = table.data[i]['U2COORD']
                        v2coord = table.data[i]['V2COORD']
                        u3coord = -u1coord - u2coord
                        v3coord = -v1coord - v2coord
                        u_coords_run.append([u1coord,u2coord,u3coord])
                        v_coords_run.append([v1coord,v2coord,v3coord])
                        time_obs_run.append(table.data[i]['MJD'])
                        eff_wave_run.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                    wl_i+=1
        
            hdu.close()
        except:
            for file in sorted(os.listdir(dir)):
                #print(file)
                ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
                if file.endswith(".oifits"):
                    
                    filename = os.path.join(dir, file)
                    print(filename)
                    hdu = fits.open(filename)
                    
                    for table in hdu:
                        
                        #if table.name=='OI_WAVELENGTH':
                        #    eff_wave.append(table.data['EFF_WAVE'][1:33])
                        ## t3phi data:
                        wl_i=1
                        if table.name=='OI_T3':
                            for i in eachindex(table.data):
                                t3 = table.data[i]['T3PHI']
                                t3err = table.data[i]['T3PHIERR']
                                t3flag = np.where(table.data[i].field('FLAG')==True)
                                t3[t3flag] = np.nan
                                t3err[t3flag] = np.nan
                                t3phi_run.append(t3)
                                t3phierr_run.append(t3err)
                                #tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                                u1coord = table.data[i]['U1COORD']
                                v1coord = table.data[i]['V1COORD']
                                u2coord = table.data[i]['U2COORD']
                                v2coord = table.data[i]['V2COORD']
                                u3coord = -u1coord - u2coord
                                v3coord = -v1coord - v2coord
                                u_coords_run.append([u1coord,u2coord,u3coord])
                                v_coords_run.append([v1coord,v2coord,v3coord])
                                time_obs_run.append(table.data[i]['MJD'])
                                eff_wave_run.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'])
                            wl_i+=1
                
                    hdu.close()
    
    t3phi_run = np.array(t3phi_run)
    t3phierr_run = np.array(t3phierr_run)
    u_coords_run = np.array(u_coords_run)
    v_coords_run = np.array(v_coords_run)
    eff_wave_run = np.array(eff_wave_run)
    time_obs_run = np.array(time_obs_run)

    print(t3phi_run.shape)
    print(u_coords_run.shape)
    print(eff_wave_run.shape)
    print(time_obs_run.shape)
    
    ### get rid of crazy values
    #idx = np.where(t3phierr>(3*np.nanmean(t3phierr)))
    #t3phi[idx]=np.nan
    #t3_std = np.nanstd(t3phi)
    #print('Standard deviation t3phi = ',t3_std)
    #idx = np.where(abs(t3phi)>(5*t3_std))
    #t3phi[idx]=np.nan
    
    ## check for t3phi corrections based on cals (another script)
    #if ftype=='chara' or ftype=='chara_old':
    #    t3phi_corrected=np.empty((0,t3phi.shape[-1]))
    #    t3phierr_corrected=np.empty((0,t3phierr.shape[-1]))
    #    eff_wave_corrected=np.empty((0,eff_wave.shape[-1]))
    #    u_coords_corrected=np.empty((0,u_coords.shape[-1]))
    #    v_coords_corrected=np.empty((0,v_coords.shape[-1]))
    #    time_obs_corrected=np.empty((0))
    #    for file in os.listdir(dir):
    #        if file.endswith("npy"):
    #            filename = os.path.join(dir, file)
    #            t3 = np.load(filename,allow_pickle=True)
    #            print(t3.shape)
    #            t3phi_corrected = np.append(t3phi_corrected,t3,axis=0)
    #            t3phierr_corrected = np.append(t3phierr_corrected,t3[1],axis=0)
    #            eff_wave_corrected = np.append(eff_wave_corrected,t3[2],axis=0)
    #            u_coords_corrected = np.append(u_coords_corrected,t3[3],axis=0)
    #            v_coords_corrected = np.append(v_coords_corrected,t3[4],axis=0)
    #            time_obs_corrected = np.append(time_obs_corrected,t3[5],axis=0)
    #    if t3phi_corrected.shape[0]>0:
    #        t3phi = t3phi_corrected
    #        t3phierr = t3phierr_corrected
    #        eff_wave = eff_wave_corrected
    #        u_coords = u_coords_corrected
    #        v_coords = v_coords_corrected
    #        time_obs = time_obs_corrected
    #        t3phi = np.concatenate([t3phi,t3phi_corrected])
    #        t3phierr = np.concatenate([t3phierr,t3phierr_corrected])
    #        eff_wave = np.concatenate([eff_wave,eff_wave_corrected])
    #        u_coords = np.concatenate([u_coords,u_coords_corrected])
    #        v_coords = np.concatenate([v_coords,v_coords_corrected])
    #        time_obs = np.concatenate([time_obs,time_obs_corrected])
    #        print('Using CORRECTED t3phi')
    #
    #    print(t3phi.shape)
    #    print(u_coords.shape)
    #    print(eff_wave.shape)
    #    print(time_obs.shape)
    correct = input('Correct t3phi? (y/[n]): ')
    if correct == 'y':
        correction_file = input('File with corrected t3phi: ')
        t3phi_corrected = np.load(correction_file)
        print(t3phi_run.shape)
        print(t3phi_corrected.shape)
        print('Using corrected t3phi')
        t3phi_run = t3phi_corrected
    std_t3phi = np.nanstd(t3phi_run)
    print('Standard deviation t3phi = ',std_t3phi)
    
    ## average channels
    avg_channels = input('Average channels together? (y/n): ')
    if avg_channels=='y':
        t3phi_filtered = []
        t3phierr_filtered = []
        eff_wave_filtered = []
        for phi,err,wl in zip(t3phi_run,t3phierr_run,eff_wave_run):
            t3phi_filtered.append(np.nanmean(phi.reshape(-1,4),axis=1))
            #t3phi_filtered.append(np.nanmedian(phi.reshape(-1,4),axis=1))
            t3phierr_filtered.append(np.nanmedian(err.reshape(-1,4),axis=1))
            #t3phierr_filtered.append(np.nanstd(phi.reshape(-1,4),axis=1))
            eff_wave_filtered.append(np.nanmean(wl.reshape(-1,4),axis=1))
        t3phi_run=np.array(t3phi_filtered)
        t3phierr_run=np.array(t3phierr_filtered)
        eff_wave_run=np.array(eff_wave_filtered)
    
    print('Minimum error = %s'%np.nanmin(t3phierr_run))
    print('Maximum error = %s'%np.nanmax(t3phierr_run))
    rid_error = input("Put a floor on low error bars? (y/[n]): ")
    
    if rid_error == 'y':
        
        #error_val = float(input("Minimum error value (deg): "))
        #idx = np.where(t3phierr<error_val)
        #t3phi[idx]=np.nan
        
        print('Setting a floor on error bars')
        #t3phi_10 = np.nanpercentile(t3phierr,0.5)
        #t3phi_90 = np.nanpercentile(t3phierr,99.5)
        
        plt.hist(np.ndarray.flatten(t3phierr_run),bins=100)
        plt.show()
        error_median = np.nanmedian(t3phierr_run)
        error_floor = error_median/2
        print('Error floor = ',error_floor)
        
        idx1 = np.where(t3phierr_run<error_floor)
        #idx2 = np.where(t3phierr>t3phi_90)
        t3phierr_run[idx1]=error_floor
        #t3phierr[idx2]=np.nan
        #t3phi[idx1]=np.nan
        #t3phi[idx2]=np.nan
        
        print('New minimum error = %s'%np.nanmin(t3phierr_run))
        print('New Maximum error = %s'%np.nanmax(t3phierr_run))

    t3phi_run = np.array(t3phi_run)
    t3phierr_run = np.array(t3phierr_run)
    u_coords_run = np.array(u_coords_run)
    v_coords_run = np.array(v_coords_run)
    eff_wave_run = np.array(eff_wave_run)
    time_obs_run = np.array(time_obs_run)

    #### plot t3phi data - 20 closing triangles for 6 telescopes
    #for t,terr in zip(t3phi,t3phierr):
    #    x=eff_wave[0]*1e6
    #    plt.errorbar(x,t,yerr=terr,fmt='.-')
    #
    #    plt.title('%s Closure Phase'%target_id)
    #    plt.show()
    #    plt.close('all')

    t3phi[run] = t3phi_run
    t3phierr[run] = t3phierr_run
    u_coords[run] = u_coords_run
    v_coords[run] = v_coords_run
    eff_wave[run] = eff_wave_run
    time_obs[run] = time_obs_run

#t3phi = np.array(t3phi)
#t3phierr = np.array(t3phierr)
#u_coords = np.array(u_coords)
#v_coords = np.array(v_coords)
#eff_wave = np.array(eff_wave)
#time_obs = np.array(time_obs)

#t3phi = np.concatenate(t3phi,axis=0)
#t3phierr = np.concatenate(t3phierr,axis=0)
#u_coords = np.concatenate(u_coords,axis=0)
#v_coords = np.concatenate(v_coords,axis=0)
#eff_wave = np.concatenate(eff_wave,axis=0)
#time_obs = np.concatenate(time_obs,axis=0)

## split spectrum
#t3phi = t3phi[:,3:]
#t3phierr = t3phierr[:,3:]
#eff_wave = eff_wave[:,3:]

print('--'*10)
print('--'*10)
print("Total DATA")
print(t3phi.shape)
print(u_coords.shape)
print(eff_wave.shape)
print(time_obs.shape)
print('--'*10)
print('--'*10)

###########
## Now give orbital elements
###########
#P_guess=float(input('period (days):'))
#T_guess=float(input('tper (MJD):'))
#e_guess=float(input('eccentricity:'))
#omega_guess=float(input('omega (deg):'))
#ratio_guess = float(input('flux ratio (f1/f2): '))
#bw_guess = float(input('bw smearing (0.004): '))
#ud_guess = float(input('UD (mas): '))

## Ups And b
print("Using UPS AND b orbital elements")
P_guess=4.617111
T_guess= 50033.553 #59507.86
## put in a T which is NOT correct:
#T_guess = 50032.720
e_guess=0.012
omega_guess=44.106+180
ratio_guess = 3000
bw_guess = 0.0
ud_guess = 1.097
ud2_guess = 0.1
semi_guess = 4.43
## semi-major should be 4.43mas
## inclination 24deg (Piskorz et al, 2017)

## Tau Boo b
#print("Using TAU BOO b orbital elements")
#P_guess=3.3124568
#e_guess=0.011
#T_guess= 56400.44
#omega_guess=113.4+180
#ratio_guess = 2000
#bw_guess = 0.02
#ud_guess = 0.85
#ud2_guess = 0.1
## semi-major should be 3.13mas
## inclination 43.5deg (Pelletier et al, 2021)

## 51 Peg b
#print("Using 51 Peg b orbital elements")
#P_guess=4.230785
#T_guess= 50001.01
#e_guess=0.013
#omega_guess=58+180
#ratio_guess = 3000
#bw_guess = 0.02
#ud_guess = 0.677
#ud2_guess = 0.1
## semi-major should be 3.41mas
## inclination 80? (Martins et al, 2015)

#P_guess=10.21296
#T_guess=52997.6813
#e_guess=0.00198
#omega_guess=102.427
#ratio_guess = 4.6
#bw_guess = 0.005
#ud_guess = 1.05
#ud2_guess = 0.6

###########
## Now compute chi-sq
###########
params_best =[]
chi2 = []

niter = 10
T_grid = np.linspace(T_guess-P_guess/2,T_guess+P_guess/2,50)

#create a set of Parameters, choose starting value and range for search
params = Parameters()
params.add('P', value=P_guess, vary=False)#min=P_guess-0.1, max=P_guess+0.1)
params.add('T', value=T_guess, vary=False)
if circular=='y':
    params.add('e', value=0.0, vary=False)
else:
    params.add('e', value=e_guess, vary=False)
params.add('A', value=0)
params.add('B', value=0)
params.add('F', value=0)
params.add('G', value=0)
params.add('ratio', value=ratio_guess, min=0)
params.add('bw', value=0, vary=False)
params.add('ud', value=ud_guess, vary=False)
params.add('ud2', value=ud2_guess, vary=False)

iternum = 0
for T_try in tqdm(T_grid):

    params['T'].value = T_try

    params_n=[]
    chi2_n = []

    for i in np.arange(niter):

        params['A'].value = np.random.uniform(-2*semi_guess,2*semi_guess)
        params['B'].value = np.random.uniform(-2*semi_guess,2*semi_guess)
        params['F'].value = np.random.uniform(-2*semi_guess,2*semi_guess)
        params['G'].value = np.random.uniform(-2*semi_guess,2*semi_guess)

        #do fit, minimizer uses LM for least square fitting of model to data
        minner = Minimizer(cp_minimizer, params, fcn_args=(t3phi,t3phierr,u_coords,v_coords,eff_wave,time_obs,number_runs,circular),nan_policy='omit')
        result = minner.minimize()
        #result = minner.leastsq(xtol=1e-5,ftol=1e-5)

        chi2_n.append(result.redchi)
        params_n.append([result.params['P'].value,result.params['T'].value,result.params['e'].value,
                         result.params['A'].value,result.params['B'].value,
                         result.params['F'].value,result.params['G'].value,
                         result.params['ratio'].value])
        
    params_n=np.array(params_n)
    chi2_n = np.array(chi2_n)

    idx = np.nanargmin(chi2_n)
    chi2.append(chi2_n[idx])
    params_best.append(params_n[idx])

chi2 = np.array(chi2)
params_best = np.array(params_best)

# write results
np.save('./orbitsearch_results/chi_sq_%s_%s.npy'%(target_id,date), chi2)
np.save('./orbitsearch_results/params_%s_%s.npy'%(target_id,date), params_best)

## plot results
with PdfPages("./orbitsearch_results/%s_%s_orbitsearch.pdf"%(target_id,date)) as pdf:
    ## first page - chisq grid
    fig = plt.figure(frameon=False)
    ax = fig.add_subplot(111)
    plt.plot(params_best[:,1], 1/chi2, '.--')
    plt.xlabel('T (MJD)')
    plt.ylabel('1/chi2')
    #plt.title('Best Fit - %s'%best_fit)
    #plt.axis('equal')
    pdf.savefig()
    plt.close()

    ##plt.scatter(bigw_file, i_file, c=1/chi_file, cmap=cm.inferno, s = 150)
    #plt.pcolormesh(bigw_results, i_results, 1/chi_sq, cmap=cm.inferno, linewidth=0, rasterized=True)
    #plt.colorbar()
    #ax.set_xlim(np.min(bigw_results),np.max(bigw_results))
    ##ax.set_xlim(0,180)
    #ax.set_ylim(np.min(i_results),np.max(i_results))
    ##ax.set_ylim(0,90)
    #plt.xlabel('$\Omega$ (deg)')
    #plt.ylabel('i (deg)')
    ##plt.title('Best Fit - %s'%best_fit)
    ##plt.axis('equal')
    #pdf.savefig()
    #plt.close()

#report_fit(result)
index = np.argmin(chi2)
print('-----RESULTS-------')
print('T = %s'%params_best[:,1][index])
print('ratio = %s'%params_best[:,-1][index])
print('redchi = %s'%chi2[index])
print('-------------------')