######################################################################
## Tyler Gardner
##
## Do a grid search on rho, inclination, and bigomega
## Developed to make a detection of hot Jupiter ups And b (also works for binaries)
######################################################################

from chara_uvcalc import uv_calc
from binary_disk_point import binary_disk_point
from binary_disks_vector import binary_disks_vector
import numpy as np
import matplotlib.pyplot as plt
from astropy import units as u
from astropy.time import Time
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.time import Time
import astropy.io.fits as fits
from lmfit import minimize, Minimizer, Parameters, Parameter, report_fit
eachindex = lambda lst: range(len(lst))
from tqdm import tqdm
import os
import matplotlib.cm as cm
import time
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib as mp
from PyAstronomy import pyasl
ks=pyasl.MarkleyKESolver()

## function to compute expected sep,PA for each time
def companion_position(params,time):
    P = params['P']
    T = params['T']
    e = params['e']
    w = params['omega']*np.pi/180
    a = params['a']
    i = params['i']*np.pi/180
    bigw = params['bigomega']*np.pi/180

    A=a*(np.cos(bigw)*np.cos(w)-np.sin(bigw)*np.cos(i)*np.sin(w))
    B=a*(np.sin(bigw)*np.cos(w)+np.cos(bigw)*np.cos(i)*np.sin(w))
    F=a*(-np.cos(bigw)*np.sin(w)-np.sin(bigw)*np.cos(i)*np.cos(w))
    G=a*(-np.sin(bigw)*np.sin(w)+np.cos(bigw)*np.cos(i)*np.cos(w))

    M = 2*np.pi/P*(time-T)
    E = ks.getE(M,e)

    X = np.cos(E)-e
    Y = np.sqrt(1-e**2)*np.sin(E)
    dec = A*X+F*Y
    ra = B*X+G*Y
    return ra,dec

## function which returns complex vis given sep, pa, flux ratio, HA, dec, UD1, UD2, wavelength
def cvis_model(params, u, v, wl, time):
    ra = companion_position(params,time)[0]
    dec = companion_position(params,time)[1]
    ratio = params['ratio']
    bw = params['bw']
    ud = params['ud']
    ud2 = params['ud2']
    
    #ul=np.array([u/i for i in wl])
    #vl=np.array([v/i for i in wl])
    ul = u/wl
    vl = v/wl
    
    vis=binary_disks_vector().binary2(ul,vl,ra,dec,ratio,ud,ud2,bw)
    return vis

## function which returns residual of model and data to be minimized
def cp_minimizer(params,cp,cp_err,u_coords,v_coords,wl,time):
    model=[]
    for item1,item2,item4,item5 in zip(u_coords,v_coords,time,wl):
        complex_vis = cvis_model(params, item1, item2, item5, item4)
        phase = (np.angle(complex_vis[0])+np.angle(complex_vis[1])+np.angle(complex_vis[2]))
        model.append(phase)
    model=np.array(model)

    ## need to do an "angle difference"
    data = cp*np.pi/180
    err = cp_err*np.pi/180
    
    diff = np.arctan2(np.sin(data-model),np.cos(data-model))
    return diff/err

## Ask the user which directory contains all files
dir=input('Path to oifits directory: ')
target_id=input('Target ID (e.g. HD_206901): ')
date = input('Date: ')

## get information from fits file
t3phi = []
t3phierr = []
u_coords = []
v_coords = []
eff_wave = []
time_obs=[]
tels = []

ftype = input('chara/chara_old? ')

beam_map = {1:'S1',2:'S2',3:'E1',4:'E2',5:'W1',6:'W2'}

if ftype=='chara':
    for file in os.listdir(dir):
        ## In mircx pipeline, reduced files end with oifits.fit if uncalibrated or viscal.fits if calibrated
        if file.endswith("_oifits.fits") or file.endswith("_viscal.fits") or file.endswith("_uvfix.fits"):

            filename = os.path.join(dir, file)
            hdu = fits.open(filename)
            oi_target = hdu[0].header['OBJECT']

            ## Check if oifits file is your target of interest
            if oi_target==target_id:
                #print(filename)
                oi_mjd = hdu[0].header['MJD-OBS']
                oi_t3 = hdu['OI_T3'].data
                oi_vis2 = hdu['OI_VIS2'].data

                ## t3phi data:
                for i in eachindex(oi_t3):
                    t3 = oi_t3[i]['T3PHI']
                    t3err = oi_t3[i]['T3PHIERR']
                    t3flag = np.where(oi_t3[i].field('FLAG')==True)
                    t3[t3flag] = np.nan
                    t3err[t3flag] = np.nan
                    t3phi.append(t3[1:33])
                    t3phierr.append(t3err[1:33])
                    tels.append([beam_map[a] for a in oi_t3[i]['STA_INDEX']])
                    u1coord = oi_t3[i]['U1COORD']
                    v1coord = oi_t3[i]['V1COORD']
                    u2coord = oi_t3[i]['U2COORD']
                    v2coord = oi_t3[i]['V2COORD']
                    u3coord = -u1coord - u2coord
                    v3coord = -v1coord - v2coord
                    u_coords.append([u1coord,u2coord,u3coord])
                    v_coords.append([v1coord,v2coord,v3coord])

                    eff_wave.append(hdu['OI_WAVELENGTH'].data['EFF_WAVE'][1:33])
                    time_obs.append(oi_mjd)
            hdu.close()
else:
    hdu = fits.open(dir)
    for table in hdu:

        #if table.name=='OI_WAVELENGTH':
        #    eff_wave.append(table.data['EFF_WAVE'][1:33])
        
        ## t3phi data:
        wl_i=1
        if table.name=='OI_T3':
            for i in eachindex(table.data):
                t3 = table.data[i]['T3PHI']
                t3err = table.data[i]['T3PHIERR']
                t3flag = np.where(table.data[i].field('FLAG')==True)
                t3[t3flag] = np.nan
                t3err[t3flag] = np.nan
                t3phi.append(t3[1:33])
                t3phierr.append(t3err[1:33])
                tels.append([beam_map[a] for a in table.data[i]['STA_INDEX']])
                u1coord = table.data[i]['U1COORD']
                v1coord = table.data[i]['V1COORD']
                u2coord = table.data[i]['U2COORD']
                v2coord = table.data[i]['V2COORD']
                u3coord = -u1coord - u2coord
                v3coord = -v1coord - v2coord
                u_coords.append([u1coord,u2coord,u3coord])
                v_coords.append([v1coord,v2coord,v3coord])
                time_obs.append(table.data[i]['MJD'])
                eff_wave.append(hdu['OI_WAVELENGTH',wl_i].data['EFF_WAVE'][1:33])
            wl_i+=1

    hdu.close()

t3phi = np.array(t3phi)
t3phierr = np.array(t3phierr)
u_coords = np.array(u_coords)
v_coords = np.array(v_coords)
eff_wave = np.array(eff_wave)
time_obs = np.array(time_obs)


print(t3phi.shape)
print(u_coords.shape)
print(eff_wave.shape)
print(time_obs.shape)

### get rid of crazy values
idx = np.where(t3phierr>(3*np.nanmean(t3phierr)))
t3phi[idx]=np.nan
t3_std = np.nanstd(t3phi)
print('Standard deviation t3phi = ',t3_std)
idx = np.where(abs(t3phi)>(3*t3_std))
t3phi[idx]=np.nan

## average channels
avg_channels = input('Average channels together? (y/n): ')
if avg_channels=='y':
    t3phi_filtered = []
    t3phierr_filtered = []
    eff_wave_filtered = []
    for phi,err,wl in zip(t3phi,t3phierr,eff_wave):
        t3phi_filtered.append(np.nanmean(phi.reshape(-1,4),axis=1))
        t3phierr_filtered.append(np.nanmean(err.reshape(-1,4),axis=1))
        #t3phierr_filtered.append(np.nanstd(phi.reshape(-1,4),axis=1))
        eff_wave_filtered.append(np.nanmean(wl.reshape(-1,4),axis=1))
    t3phi=np.array(t3phi_filtered)
    t3phierr=np.array(t3phierr_filtered)
    eff_wave=np.array(eff_wave_filtered)

print(t3phi.shape)
print(u_coords.shape)
print(eff_wave.shape)
print(time_obs.shape)

## check for t3phi corrections based on cals (another script)
if ftype=='chara':
    t3phi_corrected=np.empty((0,t3phi.shape[-1]))
    t3phierr_corrected=np.empty((0,t3phierr.shape[-1]))
    eff_wave_corrected=np.empty((0,eff_wave.shape[-1]))
    u_coords_corrected=np.empty((0,u_coords.shape[-1]))
    v_coords_corrected=np.empty((0,v_coords.shape[-1]))
    time_obs_corrected=np.empty((0))
    for file in os.listdir(dir):
        if file.endswith("npy"):
            filename = os.path.join(dir, file)
            t3 = np.load(filename,allow_pickle=True)
            t3phi_corrected = np.append(t3phi_corrected,t3[0],axis=0)
            t3phierr_corrected = np.append(t3phierr_corrected,t3[1],axis=0)
            eff_wave_corrected = np.append(eff_wave_corrected,t3[2],axis=0)
            u_coords_corrected = np.append(u_coords_corrected,t3[3],axis=0)
            v_coords_corrected = np.append(v_coords_corrected,t3[4],axis=0)
            time_obs_corrected = np.append(time_obs_corrected,t3[5],axis=0)
    if t3phi_corrected.shape[0]>0:
        t3phi = t3phi_corrected
        t3phierr = t3phierr_corrected
        eff_wave = eff_wave_corrected
        u_coords = u_coords_corrected
        v_coords = v_coords_corrected
        time_obs = time_obs_corrected
        #t3phi = np.concatenate([t3phi,t3phi_corrected])
        #t3phierr = np.concatenate([t3phierr,t3phierr_corrected])
        #eff_wave = np.concatenate([eff_wave,eff_wave_corrected])
        #u_coords = np.concatenate([u_coords,u_coords_corrected])
        #v_coords = np.concatenate([v_coords,v_coords_corrected])
        #time_obs = np.concatenate([time_obs,time_obs_corrected])
        print('Using CORRECTED t3phi')

    print(t3phi.shape)
    print(u_coords.shape)
    print(eff_wave.shape)
    print(time_obs.shape)

### plot t3phi data - 20 closing triangles for 6 telescopes
#fig,axs = plt.subplots(4,5,figsize=(15,10),facecolor='w',edgecolor='k')
#index = np.arange(20)
#
#fig.subplots_adjust(hspace=0.5,wspace=.001)
#axs=axs.ravel()
#
#for ind in index:
#    t3data=[]
#    t3errdata=[]
#    for t,terr,tri in zip(t3phi,t3phierr,tels):
#        if str(tri)==str(tels[int(ind)]):
#            t3data.append(t)
#            t3errdata.append(terr)
#    t3data=np.array(t3data)
#    t3errdata=np.array(t3errdata)
#
#    for y,yerr in zip(t3data,t3errdata):
#        x=eff_wave[0]*1e6
#        axs[int(ind)].errorbar(x,y,yerr=yerr,fmt='.-')
#    axs[int(ind)].set_title(str(tels[int(ind)]))
#
#fig.suptitle('%s Closure Phase'%target_id)
#fig.text(0.5, 0.05, 'Wavelength (micron)', ha='center')
#fig.text(0.05, 0.5, 'CP (deg)', va='center', rotation='vertical')
#plt.show()
#plt.close('all')

###########
## Now give orbital elements
###########
#P_guess=float(input('period (days):'))
#T_guess=float(input('tper (MJD):'))
#e_guess=float(input('eccentricity:'))
#omega_guess=float(input('omega (deg):'))
#ratio_guess = float(input('flux ratio (f1/f2): '))
#bw_guess = float(input('bw smearing (0.004): '))
#ud_guess = float(input('UD (mas): '))

P_guess=4.617111
T_guess=50033.55
e_guess=0.012
omega_guess=224.11
#a_guess = 4.58
#inc_guess = 25.43
#bigomega_guess = 51.92
#a_guess = 4.71
#inc_guess = 71.94
#bigomega_guess = 204.57
a_guess = 4.01
inc_guess = 35.31
bigomega_guess = 291.02

#P_guess=4.615783
#T_guess=50036.6256
#e_guess=0.024716
#omega_guess=259.9426
#a_guess = 4.6067
#inc_guess = 27.144
#bigomega_guess = 61.1912

ratio_guess = 5000
bw_guess = 0.005
ud_guess = 1.097
ud2_guess = 0.1

###########
## Now compute chi-sq
###########

spectrum = []
spectrum_err = []
xspec = []

index = np.arange(t3phi.shape[-1])
for ind in tqdm(index):

        #create a set of Parameters, choose starting value and range for search
        params = Parameters()
        params.add('P', value=P_guess, vary=False)
        params.add('T', value=T_guess, vary=False)
        params.add('e', value=e_guess, vary=False)
        params.add('omega', value=omega_guess, vary=False)
        params.add('bw', value=bw_guess, vary=False)
        params.add('ud', value=ud_guess, vary=False)
        params.add('ud2', value=ud2_guess, vary=False)
        params.add('a', value=a_guess, vary=False)
        params.add('i', value=inc_guess, vary=False)
        params.add('bigomega', value=bigomega_guess, vary=False)#min=0, max=360)
        params.add('ratio', value=ratio_guess, min=1.0)

        xspec.append(eff_wave[0,ind])
        try:
            #do fit, minimizer uses LM for least square fitting of model to data
            minner = Minimizer(cp_minimizer, params, fcn_args=(t3phi[:,ind],t3phierr[:,ind],u_coords,v_coords,eff_wave[:,ind],time_obs),nan_policy='omit')
            #result = minner.minimize()
            result = minner.leastsq(xtol=1e-5,ftol=1e-5)

            #print('ratio = %s'%result.params['ratio'].value)
            spectrum.append(1/result.params['ratio'].value)
            percent_err = result.params['ratio'].stderr / result.params['ratio'].value
            spectrum_err.append((1/result.params['ratio'].value)*percent_err)
        except:
            spectrum.append(np.nan)
            spectrum_err.append(np.nan)

spectrum = np.array(spectrum)
spectrum_err = np.array(spectrum_err)
xspec = np.array(xspec)

## plot spectrum
plt.errorbar(xspec,spectrum,yerr=spectrum_err,fmt='o')
plt.xlabel('Wavelength')
plt.ylabel('f2/f1')
plt.savefig("./orbitsearch_cp/%s_%s_spectrum.pdf"%(target_id,date))
plt.close()

## save arrays
np.save('./orbitsearch_cp/spectrum_%s_%s.npy'%(target_id,date), spectrum)
np.save('./orbitsearch_cp/spectrum_err_%s_%s.npy'%(target_id,date), spectrum_err)